#include "mainwindow.h"
#include "./ui_mainwindow.h"

#include <QFuture>
#include <QPainter>
#include <QThread>
#include <QTimer>
#include <QtConcurrent/QtConcurrent>

MainWindow::MainWindow(QWidget* parent) :
        QMainWindow(parent),
        ui(new Ui::MainWindow),
        mHeightMap(":/res/D22.png"),
        mTerrain(":/res/C22W.png") {
    ui->setupUi(this);
    mFrame = QImage(800, 600, QImage::Format_RGB32);
    ui->renderWidget->setMinimumSize(800, 600);
    ui->debugHeightmap->setScaledContents(true);
    ui->debugTerrain->setScaledContents(true);

    ui->renderWidget->setHeightMap(mHeightMap);
    ui->renderWidget->setTerrain(mTerrain);

    connect(this, &MainWindow::frameUpdated, this, &MainWindow::drawFrame);
    connect(ui->heightSlider, &QSlider::sliderReleased, this, [this]() { render(); });
    connect(ui->scaleSlider, &QSlider::sliderReleased, this, [this]() { render(); });
    connect(ui->horizonSlider, &QSlider::sliderReleased, this, [this]() { render(); });

    render();
}

MainWindow::~MainWindow() {
    delete ui;
}

void MainWindow::render() {
    ui->renderWidget->setLocation(QVector3D(512, 512, ui->heightSlider->value()));
    ui->renderWidget->setHorizonHeight(ui->heightSlider->value());
    ui->renderWidget->setHeightScale(ui->scaleSlider->value());

    QtConcurrent::run([this]() { render({512, 512}, 400); });
}

void MainWindow::render(QPoint p, int distance) {
    QMutexLocker locker(&mMutex);
    quint16 height = ui->heightSlider->value();
    int horizon = ui->horizonSlider->value();
    int scale_height = ui->scaleSlider->value();
    bool animateRender = ui->animateRenderCheckBox->isChecked();

    // Clear debug frames
    mHeightMapDebug = mHeightMap.convertToFormat(QImage::Format_RGB32);
    mTerrainDebug = mTerrain.convertToFormat(QImage::Format_RGB32);

    // 1 - Clear frame
    mFrame.fill(Qt::blue);
    QPainter painter;
    Q_ASSERT(painter.begin(&mFrame));

    // 2 - Render
    for (int d = distance; d >= 1; d--) {
        // Find line on map. This calculation corresponds to a field of view of 90°
        const QPointF leftPoint(p.x() - d, p.y() - d);
        const QPointF rightPoint(p.x() + d, p.y() - d);

        QPointF ray(leftPoint);
        // Segment the line
        qreal dx = (rightPoint.x() - leftPoint.x()) / mFrame.width();

        // Raster line and draw a vertical line for each segment
        for (int j = 0; j <= mFrame.width(); j++) {
            int projectedHeight =
                (double)(height - mHeightMap.pixelColor(ray.toPoint()).value()) / d * scale_height + horizon;

            painter.setPen(mTerrain.pixelColor(ray.toPoint()));
            debug(ray.toPoint());
            painter.drawLine(j, projectedHeight, j, mFrame.height());
            ray.setX(ray.x() + dx);
        }
        if (animateRender) {
            emit frameUpdated();
        }
    }
    emit frameUpdated();
}

void MainWindow::debug(const QPoint& p) {
    QPainter heightPainter, terrainPainter;
    Q_ASSERT(heightPainter.begin(&mHeightMapDebug));
    Q_ASSERT(terrainPainter.begin(&mTerrainDebug));
    heightPainter.setPen(QPen(QBrush(Qt::red, Qt::SolidPattern), 1));
    terrainPainter.setPen(QPen(QBrush(Qt::red, Qt::SolidPattern), 1));
    heightPainter.drawPoint(p);
    terrainPainter.drawPoint(p);
}

void MainWindow::drawFrame() const {
    ui->debugHeightmap->setPixmap(QPixmap::fromImage(mHeightMapDebug));
    ui->debugTerrain->setPixmap(QPixmap::fromImage(mTerrainDebug));
}
