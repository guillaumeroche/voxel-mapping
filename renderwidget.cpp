#include "renderwidget.h"

RenderWidget::RenderWidget(QWidget* parent) : QOpenGLWidget(parent) {
}

void RenderWidget::paintEvent(QPaintEvent* e) {
}

void RenderWidget::setLocation(const QVector3D& location) {
    mLocation = location;
}

void RenderWidget::setHorizonHeight(quint16 horizonHeight) {
    mHorizonHeight = horizonHeight;
}

void RenderWidget::setHeightScale(int heightScale) {
    mHeightScale = heightScale;
}

void RenderWidget::setHeightMap(const QImage& heightmap) {
    mHeightMap = heightmap;
}

void RenderWidget::setTerrain(const QImage& terrain) {
    mTerrain = terrain;
}
