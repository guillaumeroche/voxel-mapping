#ifndef RENDERWIDGET_H
#define RENDERWIDGET_H

#include <QOpenGLWidget>
#include <QVector3D>

class RenderWidget : public QOpenGLWidget {
public:
    RenderWidget(QWidget* parent = nullptr);

    void paintEvent(QPaintEvent* e) override;

    void setLocation(const QVector3D& location);

    void setHorizonHeight(quint16 horizonHeight);

    void setHeightScale(int heightScale);

    void setHeightMap(const QImage& heightmap);

    void setTerrain(const QImage& terrain);

private:
    QVector3D mLocation;
    quint16 mHorizonHeight;
    int mHeightScale;

    QImage mHeightMap;
    QImage mTerrain;
};

#endif // RENDERWIDGET_H
