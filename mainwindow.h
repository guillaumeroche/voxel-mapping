#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMutex>

QT_BEGIN_NAMESPACE
namespace Ui {
class MainWindow;
}
QT_END_NAMESPACE

class MainWindow : public QMainWindow {
    Q_OBJECT

public:
    MainWindow(QWidget* parent = nullptr);
    ~MainWindow();

signals:
    void frameUpdated();

private:
    void render();
    void render(QPoint p, int distance);
    void debug(const QPoint& p);
    void drawFrame() const;

    Ui::MainWindow* ui;

    const QImage mHeightMap;
    const QImage mTerrain;
    QImage mHeightMapDebug;
    QImage mTerrainDebug;
    QImage mFrame;
    QMutex mMutex;
};
#endif // MAINWINDOW_H
